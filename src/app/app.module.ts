import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './module/login/login.component';
import { AdminComponent } from './module/admin/admin.component';
import { TopNavComponent } from './module/navbar/top-nav/top-nav.component';
import { DashboardComponent } from './module/sub-menu/dashboard/dashboard.component';
import { ListCustomerComponent } from './module/sub-menu/list-customer/list-customer.component';
import { SearchCustomerComponent } from './module/sub-menu/search-customer/search-customer.component';
import { RedemptionListComponent } from './module/sub-menu/redemption-list/redemption-list.component';
import { PointTransferComponent } from './module/sub-menu/point-transfer/point-transfer.component';
import { ReportCustomerJoinComponent } from './module/sub-menu/report-customer-join/report-customer-join.component';
import { SettlementComponent } from './module/sub-menu/settlement/settlement.component';
import { PaymentComponent } from './module/sub-menu/payment/payment.component';
import { BonusFilesComponent } from './module/sub-menu/bonus-files/bonus-files.component';
import { ManualRedeemComponent } from './module/sub-menu/manual-redeem/manual-redeem.component';
import { FeedLogComponent } from './module/sub-menu/feed-log/feed-log.component';
import { ReportDetailComponent } from './module/sub-menu/report-detail/report-detail.component';
import { ReportSummaryComponent } from './module/sub-menu/report-summary/report-summary.component';
import { OfficeComponent } from './module/sub-menu/office/office.component';
import { ProductComponent } from './module/sub-menu/product/product.component';
import { ChannelComponent } from './module/sub-menu/channel/channel.component';
import { MerchantBoComponent } from './module/sub-menu/merchant-bo/merchant-bo.component';
import { EarningRulesComponent } from './module/sub-menu/earning-rules/earning-rules.component';
import { LevelsComponent } from './module/sub-menu/levels/levels.component';
import { SegmentComponent } from './module/sub-menu/segment/segment.component';
import { RewardCampaignComponent } from './module/sub-menu/reward-campaign/reward-campaign.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminComponent,
    TopNavComponent,
    DashboardComponent,
    ListCustomerComponent,
    SearchCustomerComponent,
    RedemptionListComponent,
    PointTransferComponent,
    ReportCustomerJoinComponent,
    SettlementComponent,
    PaymentComponent,
    BonusFilesComponent,
    ManualRedeemComponent,
    FeedLogComponent,
    ReportDetailComponent,
    ReportSummaryComponent,
    OfficeComponent,
    ProductComponent,
    ChannelComponent,
    MerchantBoComponent,
    EarningRulesComponent,
    LevelsComponent,
    SegmentComponent,
    RewardCampaignComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
