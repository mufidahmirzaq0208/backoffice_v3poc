import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BonusFilesComponent } from './bonus-files.component';

describe('BonusFilesComponent', () => {
  let component: BonusFilesComponent;
  let fixture: ComponentFixture<BonusFilesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BonusFilesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BonusFilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
