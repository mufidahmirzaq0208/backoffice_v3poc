import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MerchantBoComponent } from './merchant-bo.component';

describe('MerchantBoComponent', () => {
  let component: MerchantBoComponent;
  let fixture: ComponentFixture<MerchantBoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MerchantBoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MerchantBoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
