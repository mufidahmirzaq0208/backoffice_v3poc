import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportCustomerJoinComponent } from './report-customer-join.component';

describe('ReportCustomerJoinComponent', () => {
  let component: ReportCustomerJoinComponent;
  let fixture: ComponentFixture<ReportCustomerJoinComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReportCustomerJoinComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportCustomerJoinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
