import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardCampaignComponent } from './reward-campaign.component';

describe('RewardCampaignComponent', () => {
  let component: RewardCampaignComponent;
  let fixture: ComponentFixture<RewardCampaignComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RewardCampaignComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardCampaignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
