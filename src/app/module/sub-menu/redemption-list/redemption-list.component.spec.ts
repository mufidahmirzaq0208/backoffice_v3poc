import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RedemptionListComponent } from './redemption-list.component';

describe('RedemptionListComponent', () => {
  let component: RedemptionListComponent;
  let fixture: ComponentFixture<RedemptionListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RedemptionListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RedemptionListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
