import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedLogComponent } from './feed-log.component';

describe('FeedLogComponent', () => {
  let component: FeedLogComponent;
  let fixture: ComponentFixture<FeedLogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeedLogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
