import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  constructor() { }
  home = false;
  ct = false;
  mo = false;
  inf = false;
  rgl = false;
  master = false;
  mbo = false;
  er = false;
  levels = false;
  segments = false;
  rc = false;

  name_side : any;

  ngOnInit(): void {
  }

  showDropdown(event : any){
    console.log(event.target.className);
    if(event.target.className == 'home'){
      this.home = !this.home;
    }
    if(event.target.className == 'custtrack'){
      this.ct = !this.ct;
    }
    if(event.target.className == 'marcop'){
      this.mo = !this.mo;
    }
    if(event.target.className == 'inputfiles'){
      this.inf = !this.inf;
    }
    if(event.target.className == 'reportgl'){
      this.rgl = !this.rgl;
    }
    if(event.target.className == 'master'){
      this.master = !this.master;
    }
    if(event.target.className == 'merchantbo'){
      this.mbo = !this.mbo;
    }
    if(event.target.className == 'earnrul'){
      this.er = !this.er;
    }
    if(event.target.className == 'levels'){
      this.levels = !this.levels;
    }
    if(event.target.className == 'segments'){
     this.segments = !this.segments;
    }
    if(event.target.className == 'rewcamp'){
      this.rc = !this.rc;
    }
  }

  selectedNav(name : any){
    this.name_side = name;
  }


}
