import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { FormGroup, FormControl, FormBuilder} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formLogin = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  })

  data_login : any;
  status : any;
  token : any;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private route: Router,
  ) { }

  ngOnInit(): void {
    
  }

  onSubmit() {
    console.log(this.formLogin.value.username)
    let body = {
      "loginBackoffice":
        {
          "userId": this.formLogin.value.username,
          "userPassword": this.formLogin.value.password
        }
    }
    this.loginService.getToken(body).pipe().subscribe(
      res => 
      {
        this.data_login = res;
        if(this.data_login.status === 'SUCCESS'){
          localStorage.setItem('token', this.data_login.token)
          this.route.navigate(['/admin'])
        } 
        else {

        }
      }
    )
  }

}
