import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { API } from '../../api-url'
import { Observable } from 'rxjs';

@Injectable
({
  providedIn: 'root'
})

export class LoginService 
{
    constructor
    (
      private http: HttpClient
    ) { }

    public getToken(body : any){
        return this.http.post(`${API.loginBO}`, body);
    }
}