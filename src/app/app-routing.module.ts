import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './module/login/login.component';
import { AdminComponent } from './module/admin/admin.component';
import { DashboardComponent } from './module/sub-menu/dashboard/dashboard.component'
import { ListCustomerComponent } from './module/sub-menu/list-customer/list-customer.component'

const routes: Routes = [
  {path: "", redirectTo: "/login", pathMatch: "full"},
  {path: "login", component: LoginComponent },
  {path: "admin", component: AdminComponent },
  {path: "admin/dashboard", component: DashboardComponent},
  {path: "admin/list-customer", component: ListCustomerComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routingComponent = 
[
  LoginComponent,
  AdminComponent
];
